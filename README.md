# Docker Netlify Hugo
Generate static site using [gohugo](https://gohugo.io/) and deploy using [netlify-cli](https://www.netlify.com/docs/cli/).
